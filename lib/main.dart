import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Welcome to Flutter' ,
      home: Scaffold(
        appBar: AppBar(
            title: Text('Welcome to Flutter'),
        ),
        body: Center(
          child: Text('My Name is Ryann Palacio, I will graduate in the Spring of 2021, and my favorite quote is "It is Important to draw wisdom from many different places" - Uncle Iroh')
        ),
      ),
    );
  }
}